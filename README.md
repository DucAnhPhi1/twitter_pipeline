# Twitter Pipeline with Elasticsearch and Kibana

- [Motivation](#motivation)
- [Requirements](#requirements)
- [Getting started](#getting-started)
- [System Overview](#system-overview)
- [Change Twitter handles](#change-twitter-handles)
- [Notifications](#notifications)
- [Reconnection strategy](#reconnection-strategy)
- [Debugging](#debugging)
- [Getting started with Kibana](#getting-started-with-kibana)

## Motivation
The existing TWIPA twitter crawler simply stored tweet data in MongoDB. Quering and visualizing this complex json data is rather tedious.


This new Twitter pipeline harnesses the power of [Elasticsearch](https://www.elastic.co/products/elasticsearch) in combination with [Kibana](https://www.elastic.co/products/kibana). Both tools enable rich queries and simple creation of visualizations of the incoming stream of Twitter data.


The Twitter data is stored in Elasticsearch. Kibana offers a rich web interface to monitor, query and visualize the data. There is also a Python API to access Elasticsearch programmatically.

## Requirements
* Install Python 3.6
* Install dependencies with:
```
$ pip3 install -r requirements.txt
```

## Getting started

Before you can make any API request to Twitter, you’ll need to create an application at
https://dev.twitter.com/apps. Creating an application is the standard way for developers
to gain API access and for Twitter to monitor and interact with third-party platform developers
as needed

* After registering your application, at this point, you
should have a **consumer key**, **consumer secret**, **access token**, and **access token secret**.


* set the environment variables as follows:

```
$ export TWITTER_CONSUMER_KEY="your-consumer-key"

$ export TWITTER_CONSUMER_SECRET="your-consumer-secret"

$ export TWITTER_ACCESS_TOKEN="your-access-token"

$ export TWITTER_ACCESS_SECRET="your-access-secret"

```
**You are now set up!**

Run the Twitter Pipeline with:

```
python3 app.py
```
## System Overview

### Project structure

```
├── app.py
├── client.py
├── notification.py
├── README.md
├── requirements.txt
├── tweet_custom_mapping.json
├── twitter_crawler.py
├── app.log
├── resources
│   ├── abgeordnetenhaus-agh.txt
│   ├── alle-25-parteien-ep2014.txt
│   ├── alle-deutschen-mep.txt
│   ├── bundesregierung2.txt
│   ├── bundesregierung.txt
│   ├── bundestag2.txt
│   ├── bundestagsfraktionen.txt
│   ├── bundestag.txt
│   ├── dtpolitiker.txt
│   ├── mdbb-bremen.txt
│   ├── mdhb-hamburg.txt
│   ├── mdl-baden-w-rttemberg.txt
│   ├── mdl-bayern.txt
│   ├── mdl-brandenburg.txt
│   ├── mdl-hessen.txt
│   ├── mdl-mecklenburg-vorpommen.txt
│   ├── mdl-niedersachsen.txt
│   ├── mdl-nrw.txt
│   ├── mdl-rheinland-pfalz1.txt
│   ├── mdl-saarland.txt
│   ├── mdl-sachsen-anhalt.txt
│   ├── mdl-sachsen.txt
│   ├── mdl-schleswig-holstein.txt
│   └── mdl-th-ringen.txt
└──
```


## Change Twitter handles

The Twitter Pipeline fetches incoming Tweets of specified lists of users in the resources folder. For each user specified, the stream will contain:

- Tweets created by the user
- Tweets which are retweeted by the user
- Replies to any Tweet created by the user
- Retweets of any Tweet created by the user
- Manual replies, created without pressing a reply button (e.g. "@twitterapi I agree")

You can specify users you want to follow in a text file inside the resources folder. It should have the following format:

* each line represents a user with the username followed by a comma and the Twitter ID:
```
twitter_screen_name,1234567890
```

**You have to restart the Twitter Pipeline after adding new Twitter handles.**

The resources folder contains the following lists of german politician handles on twitter:

**Parteien**

- [Alle zugelassenen Parteien zur ep2014](https://twitter.com/wahl_beobachter/lists/alle-25-parteien-ep2014/members)

- [Bundestagsfraktionen](https://twitter.com/wahl_beobachter/lists/bundestagsfraktionen/members)

**Führende deutsche Politiker**

- [Führende deutsche Politiker](https://twitter.com/dpa/lists/deutschepolitiker/members?lang=de)

**Bundestag**

- [Alle twitternden Abgeordneten aus dem Deutschen Bundestag](https://twitter.com/wahl_beobachter/lists/mdb-bundestag/members)

- [Alle Abgeordneten im Deutschen Bundestag](https://twitter.com/christoph_z/lists/politik-bundestag-mdb-all/members?lang=de)

**Bundesregierung**

- [Alle twitternden Minister, Staatsminister und Staatssekretäre der Bundesregierung](https://twitter.com/wahl_beobachter/lists/bundesregierung/members)

**Deutsche Mitglieder des Europäischen Parlaments**

- [Alle deutschen Mitglieder des Europäischen Parlaments mit Twitter-Account](https://twitter.com/wahl_beobachter/lists/alle-deutschen-mep/members)

**Landestage**

- [Alle twitternden Abgeordneten aus dem Bayerischen Landtag](https://twitter.com/wahl_beobachter/lists/mdl-bayern)

- [Alle twitternden Abgeordneten aus dem Hessischen Landtag](https://twitter.com/wahl_beobachter/lists/mdl-hessen/members)

- [Alle twitternden Abgeordneten aus dem Niedersächsischen Landtag](https://twitter.com/wahl_beobachter/lists/mdl-niedersachsen/members)

- [Alle twitternden Bürgerschaftsabgeordneten aus Bremen](https://twitter.com/wahl_beobachter/lists/mdbb-bremen/members)

- [Alle twitternden Abgeordneten aus dem Landtag Brandenburg](https://twitter.com/wahl_beobachter/lists/mdl-brandenburg/members)

- [Alle twitternden Abgeordneten aus dem Sächsischen Landtag](https://twitter.com/wahl_beobachter/lists/mdl-sachsen)

- [Alle twitternden Abgeordneten aus dem Landtag Rheinland-Pfalz](https://twitter.com/wahl_beobachter/lists/mdl-rheinland-pfalz1/members)

- [Alle twitternden Abgeordneten aus dem Landtag Thüringen](https://twitter.com/wahl_beobachter/lists/mdl-th-ringen/members)

- [Alle twitternden Landtagsabgeordneten aus Sachsen-Anhalt](https://twitter.com/wahl_beobachter/lists/mdl-sachsen-anhalt)

- [Alle twitternden Landtagsabgeordneten aus Baden-Württemberg](https://twitter.com/wahl_beobachter/lists/mdl-baden-w-rttemberg/members)

- [Alle twitternden Landtagsabgeordneten aus NRW](https://twitter.com/wahl_beobachter/lists/mdl-nrw/members)

- [Alle twitternden Landtagsabgeordneten aus dem Saarland](https://twitter.com/wahl_beobachter/lists/mdl-saarland/members)

- [Alle twitternden Abgeordneten aus dem Schleswig-Holsteinischen Landtag](https://twitter.com/wahl_beobachter/lists/mdl-schleswig-holstein/members)

- [Alle twitternden Abgeordneten aus dem Landtag Mecklenburg-Vorpommen](https://twitter.com/wahl_beobachter/lists/mdl-mecklenburg-vorpommen/members)

- [Alle twitternden MdA des Berliner Abgeordnetenhauses](https://twitter.com/wahl_beobachter/lists/abgeordnetenhaus-agh)

- [Alle Mitglieder der Hamburgischen Bürgerschaft mit Twitter-Account](https://twitter.com/wahl_beobachter/lists/mdhb-hamburg/members)

## Notifications
If there is an error with the running Twitter Pipeline, the program sends a notification email with the error code to a specified recipient. You can change the sender and recipient in the file: ```notifications.py```. Notice: An error notification does not mean that the Twitter pipeline stopped running, as it might reconnect again.

## Reconnection strategy
If the stream is disconnected due to some error the program is going to immediately attempt to reconnect. If the first attempt fails, the next attempt is going to run after a 1 minute delay. After each consecutive attempt the delay is doubled until it reaches 16 minutes or a total of 6 reconnection attempts. If the 6th attempt fails, the program stops reconnecting and terminates. This reconnection strategy ensures that our Twitter Pipeline is not going to be [rate-limited](https://developer.twitter.com/en/docs/basics/rate-limiting.html) by the Twitter Streaming Api.

## Debugging
If you run into some problems with the Twitter Pipeline you should look into the log file: ```app.log```. This file contains logs of errors and other relevant states and information.

## Getting started with Kibana
Initially you need to create an index pattern, to tell Kibana which Elasticseach index you want to explore:

1. In Kibana, open **Management**, and then click **Index Patterns**.
2. If this is your first index pattern, the **Create index pattern** page opens automatically. Otherwise, click **Create index pattern** in the upper left.
3. Enter ```twitter_pipeline``` in the **Index pattern** field.
4. Click **Next step**
5. In **Configure settings** select ```created_at``` in the **Time Filter field name**
6. Click **Create index pattern**

Now, everytime you want to discover or visualize your data, you have to select ```twitter_pipeline``` as the index pattern.