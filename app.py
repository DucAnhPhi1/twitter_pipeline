from twitter_crawler import TwitterCrawler
from client import get_twitter_client
import tweepy
import glob
import logging

def get_all_uids():
    all_twitter_politician_uids = []
    filenames = glob.glob('./resources/*.txt')
    for filename in filenames:
        with open(filename) as f:
            all_twitter_politician_uids += [line.rstrip('\n').split(',')[1] for line in f]
    all_twitter_politician_uids = list(set(all_twitter_politician_uids))
    # with open('combined-list-all.txt', 'w') as f:
    #     for uid in all_twitter_politician_uids:
    #         f.write("{}\n".format(uid))
    return all_twitter_politician_uids

def get_uids_from_twitter_list(author, listname):
    client = get_twitter_client()
    members = []
    for page in tweepy.Cursor(client.list_members, author, listname).items():
        members.append(page)
    allM = [ [m.screen_name, m.id] for m in members]
    with open('./resources/{}.txt'.format(listname), 'w') as f:
        for member in allM:
            f.write("{},{}\n".format(member[0], member[1]))

if __name__ == "__main__":
    # initialize logger config
    logging.basicConfig(
        filename='app.log',
        filemode ='w',
        level=logging.INFO,
        format='%(asctime)s-%(name)s-%(levelname)s-%(message)s',
        datefmt='%d-%b-%y %H:%M:%S'
    )
    crawler = TwitterCrawler()
    crawler.crawlUsers(get_all_uids())