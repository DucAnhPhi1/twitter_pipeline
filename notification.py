from email.message import EmailMessage
from email.headerregistry import Address
import smtplib

SUBJECT = 'TWITTER PIPELINE RECEIVED AN ERROR'
SENDER = 'ru227@stud.uni-heidelberg.de'.split('@')
SENDER_NAME = 'Duc Anh Phi'
RECEIVER = 'ru227@stud.uni-heidelberg.de'.split('@')
RECEIVER_NAME = 'Duc Anh Phi'

class Notification():
    def __init__(
        self,
        subject = SUBJECT,
        receiver = RECEIVER,
        receiver_name = RECEIVER_NAME,
        sender = SENDER,
        sender_name = SENDER_NAME
    ):
        self.subject = subject
        self.receiver = receiver
        self.receiver_name = receiver_name
        self.sender = sender
        self.sender_name = sender_name

    def sendMail(self, error):
        msg = EmailMessage()
        msg['Subject'] = self.subject
        msg['From'] = Address(self.sender_name, self.sender[0], self.sender[1])
        msg['To'] = Address(self.receiver_name, self.receiver[0], self.receiver[1])
        msg.set_content(error)

        with smtplib.SMTP('extmail.urz.uni-heidelberg.de', 25) as smtp_server:
            smtp_server.send_message(msg)