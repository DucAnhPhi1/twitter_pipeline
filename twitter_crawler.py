import tweepy
from client import get_twitter_client
from typing import List
from elasticsearch import Elasticsearch
import json
import os
from notification import Notification
import logging
import datetime
import traceback
import time

class MyStreamListener(tweepy.StreamListener):
    def __init__(self):
        self.es_index = 'twitter_pipeline'
        self.es_username = ''
        self.es_secret = ''

        try:
            self.es_username = os.environ['ES_USERNAME']
            self.es_secret = os.environ['ES_SECRET']
        except KeyError:
            print('ES_* environment variables not set\n')

        self.es = Elasticsearch(
            ['https://elastic-dbs.ifi.uni-heidelberg.de'],
            http_auth=(self.es_username, self.es_secret),
            scheme='https',
            port=443
        )

        custom_mapping = {}
        with open('tweet_custom_mapping.json', encoding='utf-8') as data_file:
            custom_mapping = json.loads(data_file.read())

        settings = {
            'index_patterns': [self.es_index],
            'settings': {
                'mapping': {
                    'total_fields': {
                        'limit': 7000
                    }
                }
            },
            'mappings': custom_mapping
        }

        self.es.indices.create(
            index = self.es_index,
            ignore = 400
        )
        self.es.indices.put_template(
            name='twitter_template',
            body = settings
        )

        super().__init__()

    def on_status(self, status):
        self.es.index(
            index = self.es_index,
            doc_type = "_doc",
            body = status._json
        )
        logging.getLogger(__name__).info('Tweet created at {} was indexed'.format(status.created_at))
        print(status.text)

    def on_error(self, status_code):
        # On error send notification mail
        Notification().sendMail(status_code)
        logging.getLogger(__name__).warning(status_code)
        # returning non-False reconnects the stream,
        # with backoff strategy to avoid rate limiting
        return True


class TwitterCrawler():
    def __init__(self):
        self.myStream = tweepy.Stream(
            auth = get_twitter_client().auth,
            listener = MyStreamListener()
        )
        self.reconnect_attempts = 0
        self.reconnect_delay = 1

    def crawlUsers(self, userIds: List[str]):
        try:
            self.reconnect_attempts = 0
            self.reconnect_delay = 60
            self.myStream.filter(follow=userIds)
        except Exception as e:
            err_msg = 'Exception occurred. Reconnect attempts: {}. Reconnect delay: {} \n'.format(
                self.reconnect_attempts,
                self.reconnect_delay
            )

            logging.error(err_msg, exc_info=True)
            Notification().sendMail(err_msg + traceback.format_exc())

            self.reconnect_attempts += 1
            self.reconnect_delay = 2 * self.reconnect_delay

            # After first fail reconnect immediately
            if self.reconnect_attempts == 1:
                self.crawlUsers(userIds)
            # Reconnect with delay up to 16 minutes
            elif self.reconnect_delay <= 16*60:
                time.sleep(self.reconnect_delay)
                self.crawlUsers(userIds)